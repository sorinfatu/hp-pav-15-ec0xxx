# Fedora

## Installation

Installer boots fine. Everything seems to be recognised.

### Quirks:
- safe boot needs to be turned off in the bios and the status screen before the installation begins.
- no codecs out of the box (by design with Fedora)
- no nvidia driver in the main repo (by design)

Enable "Third Party Repository" on first setup to have access to the nvidia drivers

### As of writing this:
kernel : 5.18.18

nvidia : 515.65.01

- amd-ucode - automatically installed
- amdgpu driver - automatically installed
- nvidia - enable RPM Fusion

After enabling Third Party Repos, Fedora 36 has limited RPMFusion repos for the Nvidia driver and Steam but the full repo can be added using:

`sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm`

### Nvidia installation

With either the limited or full RPMFusion repo added just run :

`sudo dnf install akmod-nvidia xorg-x11-drv-nvidia-cuda`

This will install the driver and cuda support for the card. Once the installation is done, wait a few minutes (about 5) and reboot the machine. Or you can check if the nvidia module has been built with `modinfo -F version nvidia` If this outputs the current version of the driver, then everything is ready and you can reboot.

By default the nvidia card boots up in prime mode (no xorg config is added)

### codecs

With the full RPMFusion repo added run :

`sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin`

### Other

Appindicator support in Gnome:
`sudo dnf install gnome-shell-extension-appindicator gnome-extensions-app gnome-tweaks`

The default Flatpak remote in Fedora36 is limited but the full one can be added using : https://flatpak.org/setup/Fedora

