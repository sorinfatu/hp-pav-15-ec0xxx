# HP Pavilion Gaming Laptop 15-ec0xxx

Linux compatibility of HP Pavilion Gaming Laptop 15-ec0xxx across different distributions.

Diffrent distributions will be added over time as I have the time to test them out.
This repo will not contain any binary data or code but will be a list of observations for the different distros.

See [specs](specs.md) for generic information regarding linux on this laptop

# List of distributions

- [openSUSE](opensuse.md)
- [Archlinux](arch.md)
- ~~Ubuntu~~ - not testing this due to forced snap store but everything should work fine
- ~~elementary OS~~ - base for this is too old
- [Fedora](fedora.md)
- [POP_OS!](pop.md)
