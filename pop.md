# POP_OS!

## Installation

Installer boots fine. Everything seems to be recognised. Use the nvidia iso from the download page

### Quirks:
- safe boot needs to be turned off in the bios and the status screen before the installation begins.
- no codecs out of the box (by design )

### As of writing this:
kernel : 5.19

nvidia : 515.48.07

- amd-ucode - automatically installed
- amdgpu driver - automatically installed
- nvidia - already installed

### Nvidia installation

The driver is installed by default. The system boots by default in Nvidia only mode. Hybrid can be selected from Power section of the Gnome "systray"

### codecs

`sudo apt install -y ubuntu-restricted-extras`
