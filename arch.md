# Arch

## Installation

Installer boots fine. Everything seems to be recognised. Not going to cover a lot here as the Arch Wiki is a better source of information.

### Quirks:
- safe boot needs to be turned off in the bios and the status screen before the installation begins.

### As of writing this:
kernel : 5.19.3

nvidia : 515.65.01

- amd-ucode - needs to be installed
- amdgpu driver - needs to be installed
- nvidia - needs to be installed

### Nvidia installation

The nvidia drivers are in the main repos. For a better time use the dkms version of the driver.

`sudo pacman -S nvidia-dkms nvidia-settings nvidia-utils`

The package `nvidia-prime` can be added to that list as it provides an easy way to run apps on the nvidia card from the terminal by using "prime-run <app_name>"

By default the nvidia card boots up in prime mode (no xorg config is added)
