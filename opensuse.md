# openSUSE

## Leap 15.2

openSUSE Leap 15.2 would not boot correctly from the install iso. Suspect it might be due to an old kernel.
Would not reccomend it for this configuration since it uses an old kernel that doesn't properly support the Ryzen CPU.

## Tumbleweed

Installer boots fine. Everything seems to be recognised.

### Quirks:
- safe boot needs to be turned off in the bios and the status screen before the installation begins.
- no codecs out of the box (by design with openSUSE)
- no nvidia driver in the main repo (by design)

### As of writing this:
kernel : 5.19

nvidia : 515.65.01

- amd-ucode - automatically installed
- amdgpu driver - automatically installed
- nvidia - 

`sudo zypper ar -f https://download.nvidia.com/opensuse/tumbleweed nvidia`

`sudo zypper in x11-video-nvidiaG06`

By default the nvidia card boots up in prime mode (no xorg config is added)

### NVIDIA note

This is not needed anymore, the driver functions properly without further intervention.

~~To build the nvidia modules in the initram delete or append .old at the end for the file /etc/dracut.conf.d/60-nvidia-default.conf~~

~~Once that is done do a sudo dracut and reboot.~~

### codecs
`zypper ar -cfp 90 http://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman`

`zypper dup --from packman --allow-vendor-change`

or the easy way :

`sudo zypper in opi`

`opi codecs`


