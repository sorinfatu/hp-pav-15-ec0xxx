# Laptop Specifications

HP Pavilion Gaming Laptop 15-ec0xxx

AMD Ryzen 7 3750H with Radeon Vega Mobile Gfx (8) @ 2.300GHz
NVIDIA GeForce GTX 1650 Mobile / Max-Q

16 GB RAM

256 GB nvme SDD
1 TB HDD

1920x1080 laptop display
~~1920x1080 AOC HDMI external display~~ Not using this anymore

# Radeon driver

If the display mode is Prime (the Radeon card is used for everything except when apps are specifically routed through the nvidia card) there will be screen tearing. To avoid using xorg config files as they might (and will) interfere with Prime, the following can be used in the startup hook for any DE or WM:
`xrandr --output eDP --set TearFree on`
This will limit screen tearing while the amdgpu driver is in use.

# NVIDIA driver

NOTE: The Nvidia driver works fine as of 23-Aug-2022, notes on speciffic distros are still there but the race condition mentioned here does not seem to be an issue anymore.

The nvidia driver is usually available in the main repos or via a separate repo.
Installing via the .run file for the nvidia website is not recommended on any distribution.

While the driver mostly works ~~it has issues related to what i can only describe as a race condition when the system boots, causing X to load before the nvidia driver.~~ When this happens X.org is not able to use the nvidia card (either in hybrid or in full mode) and the card itself consumes more power than it would when it would be in use (14W vs 1 to 4W).
~~This can be mitigated by adding the nvidia module to the initram.~~

There will be a nvidia note section for each distribution with notes on how to do this.

Since most of the time it is better to stay in hybrid mode (use the iGPU unless otherwise instructed), the way to run applications on the Nvidia gpu in this mode is by default as follows: 

For vulkan applications:
`__NV_PRIME_RENDER_OFFLOAD=1 vkcube` (vkcube is an example app) Vulkan will generally default to the dGPU anyway but in case the application does not, use this

For openGL applications:
`__NV_PRIME_RENDER_OFFLOAD=1 __GLX_VENDOR_LIBRARY_NAME=nvidia glxgears` (glxgears is an example app)

# Wayland or Xorg

While most of the information out there will tell you wayland and nvidia don't mix, in the case of this laptop it sort of does.

First of, KDE wayland is generally unstable and i have had very little success with it. Gnome wayland on the other hand works perfectly with the laptop in hybrid mode, so, if you want to test wayland gnome is the way to go. (Please note that i am not using an external display)

However in case you are looking to do some gaming, i would suggest switching to Xorg as on wayland, depending on the game, you might encounter stuttering. This happens on games that struggle to hold a solid 60 FPS and is due to wayland being vsync-ed by default.

# LED indicator

As of writing this I have not found a way to make the "Mute" LED on the F6 key light up when the device is muted. The FN+F6 shortcut works but the indicator LED does not work.
